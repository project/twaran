<?php
/**
 * @file
 * video_lecture.features.inc
 */

/**
 * Implements hook_node_info().
 */
function video_lecture_node_info() {
  $items = array(
    'video_lecture' => array(
      'name' => t('Video Lecture'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

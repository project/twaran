<?php
/**
 * @file
 * twaran_core.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function twaran_core_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_flag_default_flags().
 */
function twaran_core_flag_default_flags() {
  $flags = array();
  // Exported flag: "Best Answer".
  $flags['best_answer'] = array(
    'entity_type' => 'comment',
    'title' => 'Best Answer',
    'global' => 0,
    'types' => array(
      0 => 'comment_node_doubt',
    ),
    'flag_short' => 'Mark it as Best Answer',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unmark it as Best Answer',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'api_version' => 3,
    'module' => 'twaran_core',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Favorite".
  $flags['favorite'] = array(
    'entity_type' => 'node',
    'title' => 'Favorite',
    'global' => 0,
    'types' => array(
      0 => 'doubt',
      1 => 'example',
      2 => 'h5p_content',
      3 => 'quiz',
      4 => 'video_illustration',
      5 => 'video_lecture',
    ),
    'flag_short' => 'Mark this content favorite',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unmark this content as favorite',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'question' => 0,
      'email_plain' => 0,
      'email_html' => 0,
      'email_textalt' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'twaran_core',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Follow".
  $flags['follow'] = array(
    'entity_type' => 'user',
    'title' => 'Follow',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow this User',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unfollow this User',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 'token',
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'api_version' => 3,
    'module' => 'twaran_core',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Report".
  $flags['report'] = array(
    'entity_type' => 'node',
    'title' => 'Report',
    'global' => 0,
    'types' => array(
      0 => 'article',
      1 => 'page',
      2 => 'course',
      3 => 'doubt',
      4 => 'example',
      5 => 'h5p_content',
      6 => 'matching',
      7 => 'media',
      8 => 'multichoice',
      9 => 'quiz',
      10 => 'reference_books',
      11 => 'video_illustration',
      12 => 'video_lecture',
    ),
    'flag_short' => 'Report',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Un Report',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'teaser' => 'teaser',
      'rss' => 0,
      'search_index' => 0,
      'search_result' => 0,
      'question' => 0,
      'email_plain' => 0,
      'email_html' => 0,
      'email_textalt' => 0,
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'i18n' => 0,
    'api_version' => 3,
    'module' => 'twaran_core',
    'locked' => array(
      0 => 'name',
    ),
  );
  // Exported flag: "Subscribe".
  $flags['subscribe'] = array(
    'entity_type' => 'taxonomy_term',
    'title' => 'Subscribe',
    'global' => 0,
    'types' => array(
      0 => 'subjects',
    ),
    'flag_short' => 'Subscribe',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unsubscribe',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 0,
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'api_version' => 3,
    'module' => 'twaran_core',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}

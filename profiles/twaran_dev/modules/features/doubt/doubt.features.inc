<?php
/**
 * @file
 * doubt.features.inc
 */

/**
 * Implements hook_node_info().
 */
function doubt_node_info() {
  $items = array(
    'doubt' => array(
      'name' => t('Doubt'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}

<?php
/**
 * @file
 * twaran_core.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function twaran_core_user_default_roles() {
  $roles = array();

  // Exported role: Beginner.
  $roles['Beginner'] = array(
    'name' => 'Beginner',
    'weight' => 2,
  );

  // Exported role: Competent.
  $roles['Competent'] = array(
    'name' => 'Competent',
    'weight' => 3,
  );

  // Exported role: Expert.
  $roles['Expert'] = array(
    'name' => 'Expert',
    'weight' => 5,
  );

  // Exported role: Master.
  $roles['Master'] = array(
    'name' => 'Master',
    'weight' => 6,
  );

  // Exported role: Moderator.
  $roles['Moderator'] = array(
    'name' => 'Moderator',
    'weight' => 7,
  );

  // Exported role: Proficient.
  $roles['Proficient'] = array(
    'name' => 'Proficient',
    'weight' => 4,
  );

  return $roles;
}

<?php
/**
 * @file
 * video_lecture.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function video_lecture_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: management_youtube-uploader:admin/config/media/youtube_uploader.
  $menu_links['management_youtube-uploader:admin/config/media/youtube_uploader'] = array(
    'menu_name' => 'management',
    'link_path' => 'admin/config/media/youtube_uploader',
    'router_path' => 'admin/config/media/youtube_uploader',
    'link_title' => 'YouTube Uploader',
    'options' => array(
      'attributes' => array(
        'title' => 'Adjust YouTube Account settings.',
      ),
      'identifier' => 'management_youtube-uploader:admin/config/media/youtube_uploader',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'management_media:admin/config/media',
  );
  // Exported menu link: navigation_video-lecture:node/add/video-lecture.
  $menu_links['navigation_video-lecture:node/add/video-lecture'] = array(
    'menu_name' => 'navigation',
    'link_path' => 'node/add/video-lecture',
    'router_path' => 'node/add/video-lecture',
    'link_title' => 'Video Lecture',
    'options' => array(
      'identifier' => 'navigation_video-lecture:node/add/video-lecture',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
    'customized' => 0,
    'language' => 'und',
    'menu_links_customized' => 0,
    'parent_identifier' => 'navigation_add-content:node/add',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Video Lecture');
  t('YouTube Uploader');

  return $menu_links;
}

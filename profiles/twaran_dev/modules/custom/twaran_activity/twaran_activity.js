(function ($) {

  Drupal.behaviors.twaran_activity = {
    attach: function(context, settings) {
      $('header .menu li.notification a').click(function(e) {
        if(!$('#notification-dropdown').hasClass('ajax-done')) {
          $.ajax({
            method: "POST",
            url: Drupal.settings.basePath + "user-notification",
          })
          .done(function(data) {
            $('#notification-dropdown').html(data);
            $('#notification-dropdown').addClass('ajax-done');
          });
        }
        e.preventDefault();
      });
    }
  }

})(jQuery);

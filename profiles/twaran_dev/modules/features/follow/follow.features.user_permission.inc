<?php
/**
 * @file
 * follow.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function follow_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'flag follow'.
  $permissions['flag follow'] = array(
    'name' => 'flag follow',
    'roles' => array(
      'Beginner' => 'Beginner',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'flag',
  );

  // Exported permission: 'unflag follow'.
  $permissions['unflag follow'] = array(
    'name' => 'unflag follow',
    'roles' => array(
      'Beginner' => 'Beginner',
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'flag',
  );

  return $permissions;
}

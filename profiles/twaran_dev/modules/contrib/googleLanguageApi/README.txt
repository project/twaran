Google AJAX Language API
=======================================  
This is a simple module that implements Google AJAX Language API for Transliteration of Indian languages

Supported Languages
Arabic
Bengali
Gujarati
Hindi
Kannada
Malayalam
Marathi, 
Nepali
Punjabi
Tamil
Telugu
Urdu

for more details about Google Transliteration service, please visit http://www.google.com/transliterate/

Vinay Yadav, Nagpur <vinayras@gmail.com>
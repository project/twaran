<?php
/**
 * @file
 * follow.features.inc
 */

/**
 * Implements hook_flag_default_flags().
 */
function follow_flag_default_flags() {
  $flags = array();
  // Exported flag: "Follow".
  $flags['follow'] = array(
    'entity_type' => 'user',
    'title' => 'Follow',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow this User',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Unfollow this User',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 'full',
      'token' => 'token',
    ),
    'show_as_field' => 0,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'show_on_profile' => 1,
    'access_uid' => 'others',
    'api_version' => 3,
    'module' => 'follow',
    'locked' => array(
      0 => 'name',
    ),
  );
  return $flags;

}
